package com.android.hood.thomashare;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.SignInButton;

//import com.google.android.gms.auth.api.Auth;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.auth.api.signin.GoogleSignInResult;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.SignInButton;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.OptionalPendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView appName = (TextView) findViewById(R.id.appName);
        TextView lblUsername = (TextView) findViewById(R.id.tvUsername);
        TextView tvPassword = (TextView) findViewById(R.id.tvPassword);
        TextView tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        Button btnLogin = (Button)findViewById(R.id.btnLogin);
        Button btnSignup = (Button)findViewById(R.id.btnSignup);
//         Set the dimensions of the sign-in button.
//        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
//        signInButton.setSize(SignInButton.SIZE_STANDARD);

        Typeface pacifico = Typeface.createFromAsset(getAssets(), "fonts/Pacifico-Regular.ttf");
        Typeface ralewayLight = Typeface.createFromAsset(getAssets(), "fonts/Raleway/Raleway-Light.ttf");
        Typeface ralewayRegular = Typeface.createFromAsset(getAssets(), "fonts/Raleway/Raleway-Regular.ttf");
        Typeface ralewayBold = Typeface.createFromAsset(getAssets(), "fonts/Raleway/Raleway-Bold.ttf");
        appName.setTypeface(pacifico);
        lblUsername.setTypeface(ralewayRegular);
        tvPassword.setTypeface(ralewayRegular);
        btnLogin.setTypeface(ralewayRegular);
        btnSignup.setTypeface(ralewayRegular);
    }
    public void Login(){
        Intent myIntent = new Intent(this, AddInterest.class);
        startActivity(myIntent);
    }
}
