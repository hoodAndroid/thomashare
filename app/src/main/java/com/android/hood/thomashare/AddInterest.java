package com.android.hood.thomashare;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class AddInterest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_interest);

        TextView tvMark = (TextView) findViewById(R.id.tvMark);
        TextView cbAb = (TextView) findViewById(R.id.cbAb);
        TextView cbArchi = (TextView) findViewById(R.id.cbArchi);
        TextView cbCfad = (TextView) findViewById(R.id.cbCfad);
        TextView cbEduc = (TextView) findViewById(R.id.cbEduc);
        TextView cbEng = (TextView) findViewById(R.id.cbEng);
        TextView cbHrm = (TextView) findViewById(R.id.cbHrm);
        TextView cbIT = (TextView) findViewById(R.id.cbIT);
        TextView cbLaw = (TextView) findViewById(R.id.cbLaw);
        TextView cbMed = (TextView) findViewById(R.id.cbMed);
        TextView cbMusic = (TextView) findViewById(R.id.cbMusic);
        TextView cbPhilo = (TextView) findViewById(R.id.cbPhilo);
        Button btnSubmit = (Button)findViewById(R.id.btnSubmit);

        Typeface ralewayLight = Typeface.createFromAsset(getAssets(), "fonts/Raleway/Raleway-Light.ttf");
        Typeface ralewayRegular = Typeface.createFromAsset(getAssets(), "fonts/Raleway/Raleway-Regular.ttf");

        tvMark.setTypeface(ralewayRegular);
        cbAb.setTypeface(ralewayLight);
        cbArchi.setTypeface(ralewayLight);
        cbCfad.setTypeface(ralewayLight);
        cbEduc.setTypeface(ralewayLight);
        cbEng.setTypeface(ralewayLight);
        cbHrm.setTypeface(ralewayLight);
        cbIT.setTypeface(ralewayLight);
        cbLaw.setTypeface(ralewayLight);
        cbMed.setTypeface(ralewayLight);
        cbMusic.setTypeface(ralewayLight);
        cbPhilo.setTypeface(ralewayLight);

        btnSubmit.setTypeface(ralewayLight);
    }
}
